call script.conf.bat

curl -H "Content-Type: application/json; charset=utf-8" -X DELETE http://%HOST_TOXIPROXY%:56001/proxies/jsonplaceholder.typicode.com

mvn gatling:test -f ../pom.xml -Dgatling.simulationClass=fr.erdprt.gatling.DefaultSimulation -Damk.baseUrl="http://localhost:56001/proxies" -Damk.toxiProxyHost="localhost" -Damk.toxiProxyTargetPort=80 -Damk.toxiProxyTargetHost="jsonplaceholder.typicode.com" -Damk.toxiProxyEndPointPort=65020 -Damk.toxiProxyEndPointPath=/comments/1 -Damk.toxiProxyBandwithType="4G" -Damk.userPerSecond=1 -Damk.duration="60seconds"
