/*
 * Copyright 2011-2018 GatlingCorp (https://gatling.io)
 *
 * All rights reserved.
 */

package fr.erdprt.gatling

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation extends Simulation {

  val config = AmkConfig.load()  
  
  val it = config.productIterator

  println("Test url=" + "https://" + config.toxiProxyHost + ":" + config.toxiProxyEndPointPort + config.toxiProxyEndPointPath)
  println("Test duration=" + config.duration)
  println("Test userPerSecond=" + config.duration)
  
  val httpProtocolTest = http
                            .baseUrl(AmkProcess.getProtocol(config.toxiProxyTargetPort) + "://" + config.toxiProxyHost + ":" + config.toxiProxyEndPointPort + config.toxiProxyEndPointPath)
                            .headers(Map("Host" -> config.toxiProxyTargetHost))
                            
  val scnTest = scenario("Test").exec(http("Test").get(""))

  setUp(AmkProcess.createProxyType(config.baseUrl, config.toxiProxyTargetHost, config.toxiProxyEndPointPort, config.toxiProxyBandwithType ,config.toxiProxyTargetPort),
        scnTest.inject(constantUsersPerSec(config.userPerSecond) during(config.duration)).protocols(httpProtocolTest)
        //AmkProcess.deleteProxy(config.baseUrl, config.toxiProxyTargetHost)      
  ) 

}

